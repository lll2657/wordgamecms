<?php 

namespace App;


use Psr\Log\LoggerInterface;
use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use \mysqli;
use App\models\Questions;
use App\models\Backgrounds;

class UserController
{
    public function getQuestions (Request $request, Response $response, $args) {
		$result = Questions::where('deleted',0)->orderBy('sort_num')->get();
		return $response->withJSON(['result' => $result]);
	}

	public function questionValidate (Request $request, Response $response, $args) {
		$info = $request->getQueryParams();
		if($info['question']==null){
			return $response->withJSON(['result' => "Question Blank"]);
			exit;
		}
		else if($info['img_path']==null){
			return $response->withJSON(['result' => "No image uploaded"]);
			exit;
		}
		else if($info['answer']==null){
			return $response->withJSON(['result' => "Answer Blank"]);
			exit;
		}
		else{
			return $response->withJSON(['result' => "true"]);
		}
	}

	public function newQuestion (Request $request, Response $response, $args) {
		$x = Questions::max('sort_num');
		if($x!=0){
			$y = $x+1;
		}
		else{
			$y = $x;
		}
		Questions::insert([
			'question' => "",
			'img_path' => "",
			'answer' => "",
			'sort_num' => $y,
			'deleted' => 0
		]);
		return $response->withJSON(['result' => Questions::max('id')]);
	}

	public function editQuestion (Request $request, Response $response, $args) {
		$x = json_decode($_GET['vdata']);
		Questions::where('id', $args['id'])->update(['question' => $x[0]]);
	}

	public function editImage (Request $request, Response $response, $args) {
		$x = json_decode($_GET['vdata']);
		Questions::where('id', $args['id'])->update(['img_path' => $x[0]]);
	}

	public function editAnswer (Request $request, Response $response, $args) {
		$x = json_decode($_GET['vdata']);
		Questions::where('id', $args['id'])->update(['answer' => $x[0]]);
	}

	public function editDistractors (Request $request, Response $response, $args) {
		$x = json_decode($_GET['vdata']);
		Questions::where('id', $args['id'])->update(['distractors' => $x[0]]);
	}

	public function delQuestion (Request $request, Response $response, $args) {
		Questions::where('id', $args['id'])->update(['deleted' => 1]);
	}

	public function getBackgrounds (Request $request, Response $response, $args) {
		$result = Backgrounds::where('deleted',0)->get();
		return $response->withJSON(['result' => $result]);
	}

	public function getEnabledBackground (Request $request, Response $response, $args) {
		$result = Backgrounds::where('enabled',1)->get();
		return $response->withJSON(['result' => $result]);
	}

	public function backgroundValidate (Request $request, Response $response, $args) {
		$info = $request->getQueryParams();
		if($info['menu']==null){
			return $response->withJSON(['result' => "No menu image uploaded"]);
			exit;
		}
		else if($info['game']==null){
			return $response->withJSON(['result' => "No in game image uploaded"]);
			exit;
		}
		else{
			return $response->withJSON(['result' => "true"]);
		}
	}

	public function newBackground (Request $request, Response $response, $args) {
		Backgrounds::insert([
			'menu' => "",
			'game' => "",
			'deleted' => 0,
			'enabled' => 0
		]);
		return $response->withJSON(['result' => Backgrounds::max('id')]);
	}

	public function editMenu (Request $request, Response $response, $args) {
		$x = json_decode($_GET['vdata']);
		Backgrounds::where('id', $args['id'])->update(['menu' => $x[0]]);
	}

	public function editGame (Request $request, Response $response, $args) {
		$x = json_decode($_GET['vdata']);
		Backgrounds::where('id', $args['id'])->update(['game' => $x[0]]);
	}

	public function delBackground (Request $request, Response $response, $args) {
		Backgrounds::where('id', $args['id'])->update(['deleted' => 1]);
		Backgrounds::where('id', $args['id'])->update(['enabled' => 0]);
	}

	public function enableBackground (Request $request, Response $response, $args) {
		Backgrounds::where('deleted',0)->update(['enabled' => 0]);
		Backgrounds::where('id', $args['id'])->update(['enabled' => 1]);
	}

	public function disableBackground (Request $request, Response $response, $args) {
		Backgrounds::where('id', $args['id'])->update(['enabled' => 0]);
	}

	public function editSortNum (Request $request, Response $response, $args) {
		$x = explode('_-_',$args['id'],2);
		$y = $x[1]+1;
		Questions::where('sort_num', $x[1])->update(['sort_num' => $y]);
		Questions::where('id', $x[0])->update(['sort_num' => $x[1]]);
	}

	public function editSortNumDown (Request $request, Response $response, $args) {
		$x = explode('_-_',$args['id'],2);
		$y = $x[1]-1;
		$z = Questions::max('sort_num');
		if($z>=$x[1]){
			Questions::where('sort_num', $x[1])->update(['sort_num' => $y]);
			Questions::where('id', $x[0])->update(['sort_num' => $x[1]]);
		}
	}
}

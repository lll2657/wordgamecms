<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Backgrounds extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'backgrounds';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable =   [
    //                            'username', 'password', 'lang_id', 'type', 'updated_at', 'created_at'
    //                         ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = ['password', 'remember_token'];

    // public function __construct($attributes = array())  {
    //     parent::__construct($attributes); // Eloquent

    //     Config::set('database.default', 'mysql');
    // }
}

<?php 
  session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
    <meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="http://hayageek.com/docs/uploadfile.css">
	<!-- <link rel="stylesheet" href="https://css-tricks.com/wp-content/themes/CSS-Tricks-14/style.css?v=2.5"> -->

<script type="text/javascript">
	var pathName=window.location.pathname;
    var path=pathName.slice(0, pathName.lastIndexOf("/"));
function altRows(id){
	if(document.getElementsByTagName){  
		
		var table = document.getElementById(id);  
		var rows = table.getElementsByTagName("tr"); 
		 
		for(i = 0; i < rows.length; i++){          
			if(i % 2 == 0){
				rows[i].className = "evenrowcolor";
			}else{
				rows[i].className = "oddrowcolor";
			}      
		}
	}
}
</script>
	<style>
	table.altrowstable {
	font-family: verdana,arial,sans-serif;
	font-size:13px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
}
table.altrowstable th {
	border-width: 1px;
	padding: 12px;
	border-style: solid;
	border-color: #a9c6c9;
}
table.altrowstable td {
	border-width: 1px;
	padding: 12px;
	border-style: solid;
	border-color: #a9c6c9;
}
.oddrowcolor{
	background-color:#FCFCFC;
}
.evenrowcolor{
	background-color:#F0FFFF;
}
.alignright{
	text-align: right;
}
.alignright2{
	text-align: right;
	width: 20%;
}
.alignleft{
	text-align: left;
}
.nothing{}
.nothing2{}
.hover_img a { position:relative; }
.hover_img a span { position:absolute; display:none; z-index:99; }
.hover_img a:hover span { display:block; }
.hover_img2 a { position:relative; }
.hover_img2 a span { position:absolute; display:none; z-index:99; bottom:30px;}
.hover_img2 a:hover span { display:block; }
		th, td {
		    padding: 5px;
		    text-align: center;
		}
		.enin{ display:inline-block; width:100%; }
		.tcin{ display:none;}
		.scin{ display:none;}
		body {
			padding: 0px 50px;

		}
	</style>
</head>
<body >
<div class="">
	<div class="starter-template">
		
	<div id="displayArea">
		<section id="Backgrounds">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Backgrounds (16:9)</h1>
					<div class="row">
					</div>
				</div>
			</div>
			<div id="room2"></div>
			<br>
			<p>
				<a class="btn btn-warning popcall" data-toggle="modal" data-target="#newBackground" role="button">
					<font color="white">Add Background</font>
				</a>
			</p>
		</section>

		<br>

		<section id="Questions">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Questions</h1>
					<div class="row">
					</div>
				</div>
			</div>
			<div id="room1"></div>
			<br>
			<p>
				<a class="btn btn-warning popcall" data-toggle="modal" data-target="#newQuestion" role="button">
					<font color="white">Add Question</font>
				</a>
			</p>
		</section>

		<div class="modal" id="newQuestion" tabindex="-1" role="dialog" style="display: none;">
		<div class="modal-dialog modal-lg big_dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="btn btn-primary" data-dismiss="modal" style="float: right;">Back</button>
					<h4 class="modal-title" id="newQuestionLabel">New Question</h4>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
						<div id="ipvArea">
							<div class="col-sm-12">
								<div class="panel-body"> 
								<table style="width:100%">
									<tr>
	 								   <th class="alignright2">Question:</th>
	 								   <td><p><textarea rows="4" cols="85" id="question"></textarea></td>
								    </tr>
								 	<tr>
	 								   <th class="alignright2">Image:</th>
	 								   <td><input type="text" class="form-control" id="img_path" readonly="readonly"><div id="image"></div></td>
								    </tr>
								    <tr>
	 								   <th class="alignright2">Answer:</th>
	 								   <td><p><textarea rows="4" cols="85" id="answer"></textarea></td>
								    </tr>
								    <tr>
	 								   <th class="alignright2">Distractors:</th>
	 								   <td><p><textarea rows="4" cols="85" id="distractors"></textarea></td>
								    </tr>
								</table>
								<input class="btn btn-info" id="createNewQuestion" style="float: right;" type="submit">
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="editQuestion" tabindex="-1" role="dialog" style="display: none;">
		<div class="modal-dialog modal-lg big_dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="btn btn-primary" data-dismiss="modal" style="float: right;">Back</button>
					<h4 class="modal-title" id="editQuestionLabel">Edit Question</h4>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
						<div id="ipvArea">
							<div class="col-sm-12">
								<div class="panel-body">
								<input type="hidden" id="question_id" readonly="readonly"><br>
								<table style="width:100%">
									<tr>
	 								   <th class="alignright2">Question:</th>
	 								   <td><p><textarea rows="4" cols="85" id="edit_question"></textarea></td>
								    </tr>
								 	<tr>
	 								   <th class="alignright2">Image:</th>
	 								   <td><div id="edit_image" style="display:none"></div>
	 								   <img id="current_image" style="width:304px;">
	 								   <button class="btn btn-danger" id="remove_image" style="float: right; margin:0px 5px" type="button">Remove</button>
	 								   <input type="text" class="form-control" id="edit_img_path" readonly="readonly"></td>
								    </tr>
								    <tr>
	 								   <th class="alignright2">Answer:</th>
	 								   <td><p><textarea rows="4" cols="85" id="edit_answer"></textarea></td>
								    </tr>
								    <tr>
	 								   <th class="alignright2">Distractors:</th>
	 								   <td><p><textarea rows="4" cols="85" id="edit_distractors"></textarea></td>
								    </tr>
								</table>
								<input class="btn btn-info" id="editCurrentQuestion" style="float: right;" type="submit">
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="newBackground" tabindex="-1" role="dialog" style="display: none;">
		<div class="modal-dialog modal-lg big_dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="btn btn-primary" data-dismiss="modal" style="float: right;">Back</button>
					<h4 class="modal-title" id="newBackgroundLabel">New Background</h4>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
						<div id="ipvArea">
							<div class="col-sm-12">
								<div class="panel-body"> 
								<table style="width:100%">
									<tr>
	 								   <th class="alignright2">Menu:</th>
	 								   <td><input type="text" class="form-control" id="menu" readonly="readonly"><div id="image1"></div></td>
								    </tr>
								 	<tr>
	 								   <th class="alignright2">In Game:</th>
	 								   <td><input type="text" class="form-control" id="game" readonly="readonly"><div id="image2"></div></td>
								    </tr>
								</table>
								<input class="btn btn-info" id="createNewBackground" style="float: right;" type="submit">
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="editBackground" tabindex="-1" role="dialog" style="display: none;">
		<div class="modal-dialog modal-lg big_dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="btn btn-primary" data-dismiss="modal" style="float: right;">Back</button>
					<h4 class="modal-title" id="editBackgroundLabel">Edit Background</h4>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
						<div id="ipvArea">
							<div class="col-sm-12">
								<div class="panel-body"> 
								<input type="hidden" id="background_id" readonly="readonly"><br>
								<table style="width:100%">
									<tr>
	 								   <th class="alignright2">Menu:</th>
	 								   <td><div id="edit_image1" style="display:none"></div>
	 								   <img id="current_image1" style="width:304px;">
	 								   <button class="btn btn-danger" id="remove_image1" style="float: right; margin:0px 5px" type="button">Remove</button>
	 								   <input type="text" class="form-control" id="edit_menu" readonly="readonly"></td>
								    </tr>
								 	<tr>
	 								  <th class="alignright2">In Game:</th>
	 								   <td><div id="edit_image2" style="display:none"></div>
	 								   <img id="current_image2" style="width:304px;">
	 								   <button class="btn btn-danger" id="remove_image2" style="float: right; margin:0px 5px" type="button">Remove</button>
	 								   <input type="text" class="form-control" id="edit_game" readonly="readonly"></td>
								    </tr>
								</table>
								<input class="btn btn-info" id="editCurrentBackground" style="float: right;" type="submit">
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script src="https://code.jquery.com/jquery-1.11.3.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>
<script src="http://hayageek.com/docs/jquery.uploadfile.min.js"></script>
<script id="entry-template" type="text/x-handlebars-template">
  <div class="entry">
    <h1>{{title}}</h1>
    <div class="body">
      {{body}}
    </div>
  </div>
</script>

<script id="question-table" type="text/x-handlebars-template">
	<table id="questions_table" class=altrowstable style="width:100%">
		<tr>
			<th>Question ID</th>
			<th>Sort</th>
			<th>Question</th>
			<th>Image</th>
			<th>Answer</th>
			<th>Disctractors</th>
			<th>Functions</th>
			<th>Sorting</th>
		</tr>
		{{#each .}}
		<tr>
			<td>{{id}}</td>
			<td>{{sort_num}}</td>
			<td>{{question}}</td>
			<td><img src="images/{{img_path}}" style="width:304px;" /></td>
			<td>{{answer}}</td>
			<td>{{distractors}}</td>
			<td><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#editQuestion" onclick="editQuestion(this);" >Edit Question</button><br><br>
			<button type="button" class="btn btn-danger btn-block" onclick="delQuestion(this);" >Delete Question</button></td>
			<td><button type="button" class="btn btn-info glyphicon glyphicon-arrow-up" onclick="moveUp(this);"></button><br><br>
			<button type="button" class="btn btn-info glyphicon glyphicon-arrow-down" onclick="moveDown(this);"></button></td>
		</tr> 
 		{{/each}}
	</table>
</script>
<script id="background-table" type="text/x-handlebars-template">
	<table id="backgrounds_table" class=altrowstable style="width:100%">
		<tr>
			<th>Background ID</th>
			<th>Menu</th>
			<th>Game</th>
			<th>Functions</th>
		</tr>
		{{#each .}}
		<tr>
			<td>{{id}}</td>
			<td><img src="bg/{{menu}}" style="width:304px;" /></td>
			<td><img src="bg/{{game}}" style="width:304px;" /></td>
			<td>
			<button type="button" class="btn btn-info btn-block" onclick="enableBackground(this);" {{see}}>Enable</button><br><br>
			<button type="button" class="btn btn-danger btn-block" onclick="disableBackground(this);" {{notSee}}>Disable</button><br><br>
			<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#editBackground" onclick="editBackground(this);" >Edit Background</button><br><br>
			<button type="button" class="btn btn-danger btn-block" onclick="delBackground(this);" >Delete Background</button></td>
		</tr> 
 		{{/each}}
	</table>
</script>

<script>
function loadQuestions() {
var room1 = $('#room1');
var renderTemplate1 = Handlebars.compile($('#entry-template').html());
var questions_table = Handlebars.compile($('#question-table').html());
	$.get(path+'/questions', function (res) {
	$('#room1').html(questions_table(res.result));
	altRows('questions_table');
});
};
function loadBackgrounds() {
var room2 = $('#room2');
var renderTemplate1 = Handlebars.compile($('#entry-template').html());
var backgrounds_table = Handlebars.compile($('#background-table').html());
	$.get(path+'/backgrounds', function (res) {
	var tableData = res.result;
	for(var i=0;i<res.result.length;i++){
		if(tableData[i].enabled===1){
			tableData[i]['see']="disabled";
			tableData[i]['notSee']="";
		}
		else{
			tableData[i]['see']="";
			tableData[i]['notSee']="disabled";
		}
	}
	$('#room2').html(backgrounds_table(tableData));
	altRows('backgrounds_table');
});
};
$(document).ready(function(e){
	loadQuestions();
	loadBackgrounds();
	var uploadObj = $("#image").uploadFile({
				url:path+"/image.php",
				allowedTypes:"png,gif,jpg,jpeg",
				fileName:"myfile",
				dragDrop:true,
				showPreview:true,
 				previewHeight: "50%",
 				previewWidth: "50%",
 				showDelete: true,
 				multiple:false,
 				maxFileCount:1,
 				onSuccess:function(files,data,xhr)
				{
					document.getElementById("image").style = "display: none";
					if(data){
						$("#img_path").val(data);
						}
					},		
				deleteCallback: function (data, pd) {
					document.getElementById("image").style = "display: blocks";
					document.getElementById("img_path").value = "";
					alert("File Deleted");
				    for (var i = 0; i < data.length; i++) {
				        $.post(path+"/delete.php", {op: "delete",name: data[i]});
				    }
				    pd.statusbar.hide();

				}
	}); 
	$("#edit_image").uploadFile({
				url:path+"/image.php",
				allowedTypes:"png,gif,jpg,jpeg",
				fileName:"myfile",
				dragDrop:true,
				showPreview:true,
 				previewHeight: "50%",
 				previewWidth: "50%",
 				showDelete: true,
 				multiple:false,
 				maxFileCount:1,
 				onSuccess:function(files,data,xhr)
				{
					document.getElementById("edit_image").style = "display: none";
					if(data){
						$("#edit_img_path").val(data);
						}
					},		
				deleteCallback: function (data, pd) {
					document.getElementById("edit_image").style = "display: blocks";
					document.getElementById("edit_img_path").value = "";
					alert("File Deleted");
				    for (var i = 0; i < data.length; i++) {
				        $.post(path+"/delete.php", {op: "delete",name: data[i]});
				    }
				    pd.statusbar.hide();

				}
	}); 
	var uploadObj1 = $("#image1").uploadFile({
				url:path+"/background.php",
				allowedTypes:"png,gif,jpg,jpeg",
				fileName:"myfile",
				dragDrop:true,
				showPreview:true,
 				previewHeight: "50%",
 				previewWidth: "50%",
 				showDelete: true,
 				multiple:false,
 				maxFileCount:1,
 				onSuccess:function(files,data,xhr)
				{
					document.getElementById("image1").style = "display: none";
					if(data){
						$("#menu").val(data);
						}
					},		
				deleteCallback: function (data, pd) {
					document.getElementById("image1").style = "display: blocks";
					document.getElementById("menu").value = "";
					alert("File Deleted");
				    for (var i = 0; i < data.length; i++) {
				        $.post(path+"/delete.php", {op: "delete",name: data[i]});
				    }
				    pd.statusbar.hide();

				}
	}); 
	$("#edit_image1").uploadFile({
				url:path+"/background.php",
				allowedTypes:"png,gif,jpg,jpeg",
				fileName:"myfile",
				dragDrop:true,
				showPreview:true,
 				previewHeight: "50%",
 				previewWidth: "50%",
 				showDelete: true,
 				multiple:false,
 				maxFileCount:1,
 				onSuccess:function(files,data,xhr)
				{
					document.getElementById("edit_image1").style = "display: none";
					if(data){
						$("#edit_menu").val(data);
						}
					},		
				deleteCallback: function (data, pd) {
					document.getElementById("edit_image1").style = "display: blocks";
					document.getElementById("edit_menu").value = "";
					alert("File Deleted");
				    for (var i = 0; i < data.length; i++) {
				        $.post(path+"/delete.php", {op: "delete",name: data[i]});
				    }
				    pd.statusbar.hide();

				}
	}); 
	var uploadObj2 = $("#image2").uploadFile({
				url:path+"/background.php",
				allowedTypes:"png,gif,jpg,jpeg",
				fileName:"myfile",
				dragDrop:true,
				showPreview:true,
 				previewHeight: "50%",
 				previewWidth: "50%",
 				showDelete: true,
 				multiple:false,
 				maxFileCount:1,
 				onSuccess:function(files,data,xhr)
				{
					document.getElementById("image2").style = "display: none";
					if(data){
						$("#game").val(data);
						}
					},		
				deleteCallback: function (data, pd) {
					document.getElementById("image2").style = "display: blocks";
					document.getElementById("game").value = "";
					alert("File Deleted");
				    for (var i = 0; i < data.length; i++) {
				        $.post(path+"/delete.php", {op: "delete",name: data[i]});
				    }
				    pd.statusbar.hide();

				}
	}); 
	$("#edit_image2").uploadFile({
				url:path+"/background.php",
				allowedTypes:"png,gif,jpg,jpeg",
				fileName:"myfile",
				dragDrop:true,
				showPreview:true,
 				previewHeight: "50%",
 				previewWidth: "50%",
 				showDelete: true,
 				multiple:false,
 				maxFileCount:1,
 				onSuccess:function(files,data,xhr)
				{
					document.getElementById("edit_image2").style = "display: none";
					if(data){
						$("#edit_game").val(data);
						}
					},		
				deleteCallback: function (data, pd) {
					document.getElementById("edit_image2").style = "display: blocks";
					document.getElementById("edit_game").value = "";
					alert("File Deleted");
				    for (var i = 0; i < data.length; i++) {
				        $.post(path+"/delete.php", {op: "delete",name: data[i]});
				    }
				    pd.statusbar.hide();

				}
	}); 
	$("#createNewQuestion").on("click",function(el){
		var question = [];
		question[0] = $("#question").val();
		var img_path = [];
		img_path[0] = $("#img_path").val();
		var answer = [];
		answer[0] = $("#answer").val();
		var distractors = [];
		distractors[0] = $("#distractors").val();
		$.get(path+'/questionValidate?question='+question[0]+'&img_path='+img_path[0]+'&answer='+answer[0], function (res2) {
			if(res2.result!=='true'){
				alert(res2.result);
			}
			else{
		$.get(path+'/newQuestion', function (res) {
			var x=res.result;
			$.get(path+'/editQuestion/' + x, {vdata: JSON.stringify(question)});
			$.get(path+'/editImage/' + x, {vdata: JSON.stringify(img_path)});
			$.get(path+'/editAnswer/' + x, {vdata: JSON.stringify(answer)});
			$.get(path+'/editDistractors/' + x, {vdata: JSON.stringify(distractors)});
			loadQuestions();
			$('#newQuestion').modal('hide')
			document.getElementById("question").value = "";
			document.getElementById("img_path").value = "";
			document.getElementById("answer").value = "";
			document.getElementById("distractors").value = "";
			document.getElementById("image").style = "display:block";
			uploadObj.reset();
		});
	}
});
	});
	$("#editCurrentQuestion").on("click",function(el){
		var edit_question = [];
		edit_question[0] = $("#edit_question").val();
		var edit_img_path = [];
		edit_img_path[0] = $("#edit_img_path").val();
		var edit_answer = [];
		edit_answer[0] = $("#edit_answer").val();
		var edit_distractors = [];
		edit_distractors[0] = $("#edit_distractors").val();
		var x=$("#question_id").val();
		$.get(path+'/editQuestion/' + x, {vdata: JSON.stringify(edit_question)});
		$.get(path+'/editImage/' + x, {vdata: JSON.stringify(edit_img_path)});
		$.get(path+'/editAnswer/' + x, {vdata: JSON.stringify(edit_answer)});
		$.get(path+'/editDistractors/' + x, {vdata: JSON.stringify(edit_distractors)});
		$('#editQuestion').modal('hide');
		loadQuestions();
	});
	$("#remove_image").on("click",function(el){
		document.getElementById("current_image").style = "display:none";
		document.getElementById("edit_img_path").value = "";
		document.getElementById("edit_image").style = "display:block";
		document.getElementById("remove_image").style = "display:none";
	});
	$("#createNewBackground").on("click",function(el){
		var menu = [];
		menu[0] = $("#menu").val();
		var game = [];
		game[0] = $("#game").val();
		$.get(path+'/backgroundValidate?menu='+menu[0]+'&game='+game[0], function (res2) {
			if(res2.result!=='true'){
				alert(res2.result);
			}
			else{
		$.get(path+'/newBackground', function (res) {
			var x=res.result;
			$.get(path+'/editMenu/' + x, {vdata: JSON.stringify(menu)});
			$.get(path+'/editGame/' + x, {vdata: JSON.stringify(game)});
			loadBackgrounds();
			$('#newBackground').modal('hide')
			document.getElementById("menu").value = "";
			document.getElementById("game").value = "";
			document.getElementById("image1").style = "display:block";
			document.getElementById("image2").style = "display:block";
			uploadObj1.reset();
			uploadObj2.reset();
		});
	}
});
	});
	$("#editCurrentQuestion").on("click",function(el){
		var edit_menu = [];
		edit_menu[0] = $("#edit_menu").val();
		var edit_game = [];
		edit_game[0] = $("#edit_game").val();
		var x=$("#background_id").val();
		$.get(path+'/editMenu/' + x, {vdata: JSON.stringify(edit_menu)});
		$.get(path+'/editGame/' + x, {vdata: JSON.stringify(edit_game)});
		$('#editBackground').modal('hide');
		loadBackgrounds();
	});
	$("#remove_image1").on("click",function(el){
		document.getElementById("current_image1").style = "display:none";
		document.getElementById("edit_menu").value = "";
		document.getElementById("edit_image1").style = "display:block";
		document.getElementById("remove_image1").style = "display:none";
	});
	$("#remove_image2").on("click",function(el){
		document.getElementById("current_image2").style = "display:none";
		document.getElementById("edit_game").value = "";
		document.getElementById("edit_image2").style = "display:block";
		document.getElementById("remove_image2").style = "display:none";
	});
});
function editQuestion(el) {
		var par = el.parentNode.parentNode;
		var children = par.children;
		document.getElementById("question_id").value = children[0].innerHTML;
		document.getElementById("edit_question").value = children[2].innerHTML;
		document.getElementById("edit_img_path").value = children[3].children[0].src.split("/").pop();
		document.getElementById("current_image").src = children[3].children[0].src;
		document.getElementById("edit_answer").value = children[4].innerHTML;
		document.getElementById("edit_distractors").value = children[5].innerHTML;
		document.getElementById("current_image").style = "width:304px;";
		document.getElementById("edit_image").style = "display:none";
		document.getElementById("remove_image").style = "display:block";
};
function delQuestion(el) {
	var r = confirm("Are you sure you want to delete?");
	if(r==true){
		var par = el.parentNode.parentNode;
		var children = par.children;
		var x = children[0].innerHTML;
		$.get(path+'/delQuestion/' + x);
		loadQuestions();
	}
};
function editBackground(el) {
		var par = el.parentNode.parentNode;
		var children = par.children;
		document.getElementById("edit_menu").value = children[1].children[0].src.split("/").pop();
		document.getElementById("current_image1").src = children[1].children[0].src;
		document.getElementById("edit_game").value = children[2].children[0].src.split("/").pop();
		document.getElementById("current_image2").src = children[2].children[0].src;
		document.getElementById("current_image1").style = "width:304px;";
		document.getElementById("edit_image1").style = "display:none";
		document.getElementById("remove_image1").style = "display:block";
		document.getElementById("current_image2").style = "width:304px;";
		document.getElementById("edit_image2").style = "display:none";
		document.getElementById("remove_image2").style = "display:block";
};
function delBackground(el) {
	var r = confirm("Are you sure you want to delete?");
	if(r==true){
		var par = el.parentNode.parentNode;
		var children = par.children;
		var x = children[0].innerHTML;
		$.get(path+'/delBackground/' + x);
		loadBackgrounds();
	}
};
function enableBackground(el) {
	var par = el.parentNode.parentNode;
	var children = par.children;
	var x = children[0].innerHTML;
	$.get(path+'/enableBackground/' + x, function (res) {
		loadBackgrounds();
	});
};
function disableBackground(el) {
	var par = el.parentNode.parentNode;
	var children = par.children;
	var x = children[0].innerHTML;
	$.get(path+'/disableBackground/' + x, function (res) {
		loadBackgrounds();
	});
};
function moveUp(el){
	var par = el.parentNode.parentNode;
	var children = par.children;
	var x = children[0].innerHTML;
	var y = parseInt(children[1].innerHTML)-1
	if(children[1].innerHTML!=="1"){
		$.get(path+'/editSortNum/' + x + '_-_' + y, function(e){
			loadQuestions();
		});
	}
};
function moveDown(el){
	var par = el.parentNode.parentNode;
	var children = par.children;
	var x = children[0].innerHTML;
	var y = parseInt(children[1].innerHTML)+1
	$.get(path+'/editSortNumDown/' + x + '_-_' + y, function(e){
		loadQuestions();
	});
}
</script>
</body>
</html>
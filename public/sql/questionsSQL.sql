-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Aug 18, 2016 at 11:57 AM
-- Server version: 5.6.25
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `word_game`
--

-- --------------------------------------------------------

--
-- Table structure for table `backgrounds`
--

CREATE TABLE `backgrounds` (
  `id` bigint(19) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `game` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `backgrounds`
--

INSERT INTO `backgrounds` (`id`, `menu`, `game`, `deleted`, `enabled`) VALUES
(1, '1470800284.png', '1470800291.png', 0, 0),
(2, '1471503930.png', '1471503940.png', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(19) NOT NULL,
  `question` varchar(300) NOT NULL,
  `img_path` varchar(100) NOT NULL,
  `answer` varchar(300) NOT NULL,
  `distractors` varchar(300) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `sort_num` bigint(19) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `img_path`, `answer`, `distractors`, `deleted`, `sort_num`) VALUES
(2, 'What is this?', '1470016865.png', 'tree', 'abc', 0, 1),
(4, 'What is this animal?', '1470017026.png', 'cat', 'xyz', 0, 2),
(5, 'What is this animal?', '1470021551.png', 'rabbit', 'edu', 0, 4),
(6, 'What is this animal?', '1470021579.png', 'dog', 'hkt', 0, 3),
(7, 'What is this animal?', '1470189694.png', 'mouse', 'drp', 0, 5),
(8, 'What is this animal?', '1470189796.png', 'pig', 'wer', 0, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backgrounds`
--
ALTER TABLE `backgrounds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backgrounds`
--
ALTER TABLE `backgrounds`
  MODIFY `id` bigint(19) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(19) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
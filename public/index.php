<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
(new Dotenv(__DIR__ . '/..'))->load();

// Instantiate the app
$settings = [
	'settings' => [
    'displayErrorDetails' => true, // set to false in production
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header
	'determineRouteBeforeAppMiddleware' => false,

    // Renderer settings
    'renderer' => [
        'template_path' => __DIR__ . '/../templates/',
    ],

    // Monolog settings
    'logger' => [
        'name' => 'slim-app',
        'path' => __DIR__ . '/../logs/app.log',
    ],
   ],
];

$app = new \Slim\App($settings);

$container = $app->getContainer();

$settingsdb = array(
    'driver' => getenv('db_driver') ?: 'mysql',
    'host' => getenv('db_host') ?: 'localhost:8889',
    'database' => getenv('db_database') ?: 'word_game',
    'username' => getenv('db_username') ?: 'root',
    'password' => getenv('db_password') ?: 'root',
    'charset'   => getenv('db_charset') ?: 'utf8',
    'collation' => getenv('db_collation') ?: 'utf8_unicode_ci',
    'prefix'    => getenv('db_prefix') ?: '',
);

// Bootstrap Eloquent ORM
$containerDB = new Illuminate\Container\Container;
$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory($containerDB);
$conn = $connFactory->make($settingsdb);
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);

$app->get('/questions', '\App\UserController:getQuestions');

$app->get('/newQuestion', '\App\UserController:newQuestion');

$app->get('/questionValidate', '\App\UserController:questionValidate');

$app->get('/editQuestion/{id}', '\App\UserController:editQuestion');

$app->get('/editImage/{id}', '\App\UserController:editImage');

$app->get('/editAnswer/{id}', '\App\UserController:editAnswer');

$app->get('/editDistractors/{id}', '\App\UserController:editDistractors');

$app->get('/delQuestion/{id}', '\App\UserController:delQuestion');

$app->get('/backgrounds', '\App\UserController:getBackgrounds');

$app->get('/background', '\App\UserController:getEnabledBackground');

$app->get('/newBackground', '\App\UserController:newBackground');

$app->get('/backgroundValidate', '\App\UserController:backgroundValidate');

$app->get('/editMenu/{id}', '\App\UserController:editMenu');

$app->get('/editGame/{id}', '\App\UserController:editGame');

$app->get('/delBackground/{id}', '\App\UserController:delBackground');

$app->get('/enableBackground/{id}', '\App\UserController:enableBackground');

$app->get('/disableBackground/{id}', '\App\UserController:disableBackground');

$app->get('/editSortNum/{id}', '\App\UserController:editSortNum');

$app->get('/editSortNumDown/{id}', '\App\UserController:editSortNumDown');

$app->run();
